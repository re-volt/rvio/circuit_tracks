Jimsonweeds Raceway by Zeino (Tryxn on RVZ)

hello all, i felt like making a race track so i made one over the past weekend. i hope you enjoy, tried my best.

featuring:
- rain version (works instead of reverse)
- practice star (one in the rain version aswell)
- time trial times to beat for both versions
- smooth operator, smooth raceline

credits:
song: Bliss by CyniR (https://www.newgrounds.com/audio/listen/1076722)
textures: various sources (Saffron's Dark Vale Forest, some textures edited by me, one texture from duc's Tropic Raceway, stickers/sponsors credit goes to Yukes von Faust, Phimeek, thatmotorfreak and Shara from their sponsor releases in the #resources channel in the discord)
rain version: duc
skybox and sounds: taken from Saffron's Dark Vale Forest

Special thanks:
Saff, my friends At Home and you idk lol