Wildwood Grove by Zeino (Tryxn on RVZ)

hi there, i felt like making a race track once again so i made one over the past week. i hope you enjoy!

featuring:
- practice star (one in the rain version aswell)
- time trial time to beat
- smooth raceline
- realistic pitstop

credits:
textures: various sources (Saffron's Dark Vale Forest, some textures edited by me, stickers/sponsors credit goes to Yukes von Faust, Phimeek, thatmotorfreak and Shara from their sponsor releases in the #resources channel in the discord)
sounds: 
taken from Saffron's Dark Vale Forest,
birds sound by klankbeeld on freesound.org (https://freesound.org/people/klankbeeld/sounds/530415/)
woodpecker sound by by InspectorJ on freesound.org (https://freesound.org/people/InspectorJ/sounds/418737/)
skybox: taken from Marv's skybox pack, author of the skybox itself is Zachery "skiingpenguins" Slocum

Special thanks:
Saff, my friends At Home and you idk lol