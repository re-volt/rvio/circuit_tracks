Track Name  : Grand Prix de Morvan
Length      : 230m
Type        : Grand prix track
------------------------------------
Author      : Volt General
Email       : voltrevolt@hotmail.com
------------------------------------

Description:
============

Again a GP track for those GP lovers there.
A bit short at 230m, but it has a fun layout.
The track could be challenging in multiplayer,
in single player alone the cars with high grip
could win... 

------------------------------------

How to Install:
===============

Unzip the complete package in your Revolt directory.

------------------------------------

Credits/Thanks to:
==================

JimK, BurnRubr, Spaceman, DSL_Tile and SuperTard
for prms and textures.

------------------------------------

Comment!!