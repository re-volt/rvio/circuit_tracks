Information
-----------------
Track Name:	Clubman Stage Route 5
Length:		480 meters
Difficulty		Easy
Author:		Saffron
Challenge time:	27:000 (Stock Pros)


Description
-----------------
Alongside Special Stage Route 5 also exists the shorter Clubman variant. You'll want to maximise your consistency throughout each crucial corner if you want to be above the rest on this mostly full throttle circuit.

Requirements
-----------------
You MUST use the latest RVGL patch to see the textures, and for the custom properties to work.


Credits
-----------------
The Internet and Polyphony Digital for the textures
Scloink (I think) for the truck model
The Blender Foundation for Blender
Marv for his Blender plug-in
Huki and Marv (again) for RVGL
ARM for the Camera nodes tutorial
Rick Brewster for Paint.NET


Permissions
-----------------
You may not redistribute or modify this track. You may not bundle this track into any content pack I am not involved with. You may not archive older versions of this track and redistribute those either.