Bijou Speedzone by Zeino (Tryxn on RVZ)

decided to make a shorter track than usual, might release a few similar to this if i feel like it, enjoy.

featuring:
- tricky narrow raceline
- practice star
- time trial time to beat
- cool pitlane

credits:
song: Distracted by Cyllum (https://www.newgrounds.com/audio/listen/1089092)
textures: various sources (Saffron's Dark Vale Forest, Super Speedway; stickers/sponsors credit goes to Yukes von Faust, Phimeek, thatmotorfreak and Shara from their sponsor releases in the #resources channel in the discord)
skybox: Luke.RUSTLTD released under CC0 on opengameart.org
sounds: taken from Saffron's Dark Vale Forest

special thanks:
Saff, my friends At Home and you idk lol