Track Name : GP1
Folder Name:GP1
Track Type : EXTREME
Author/s   : Fabit
Email      : fab60@libero.it
  
Length:1223 meters
Skymap: none

Install: Unzip with file names on to the main ReVolt folder

Tools used: Glue6 by ali; RV-Remap Rv-Sizer by chaos ; Prm2ncp
	    RVMINIS by Gabor Varga - Alexander Kroeller
	    Jimk's kit		

================================================================
Description
================================================================
Gp1 is a fast track (good for cars speed about 55 to 75 mph)
To make track body various i used instances from prmbend and Jim's kit(Atlanta track's texture),
and this mix and resize action generates some collision problem in oftrack section. 

The track is available in two different surfaces propertyes.
Default track have dry surfaces , wet surfaces and relative files are in Gp1\rain directory.
To restore dry file set you must unzip again from GP1.zip main file.
Testing my track i use a set of modified cars( re-size, re-parameter and some re-texture) 
this set i called GT2 is available in GP1\GT2 directory.
If you want copy GT2 cars folders in cars directory of re-volt, these cars race wonderful in GP1!
================================================================
Tips
In GP1c.bmp file there is the texture of large minitor in box area that is a semplified  GP1 tracing , look it.
At the last turn before finish line you can make a visit in box area for a pickups recharge.
================================================================




Thanks to all re-volt fans.
Credits:
========
-* Hilaire9 for hints 
-* Spaceman for a Re-Ville prm.
-* Ddraser for surface floor
-* Gibbler for Warehouse's prms and textures
-* Acclaim for prms and textures.
-* Ali for RV-Glue 6.
-* Mike 454 for prms & textures
-* Jimk for prms ,textures and kit.
-* Volt general or Sloink for ground texture
-* Chaos for rv-sizer ,rv-remap 
-* Gabor Varga - Alexander Kroeller for RVminis
-* J.A. for wall textures
-* Shawn64,Scloink & SuperTard, Gabor Varga  & Jamada for prms & textures

Then i want to thank the staff of RVA for their great site
also Gibbler, thanks so much for your RV Extreme website. 
If I have left anyone out I am sorry and feel free to remember me 
for it .
Thanks to all re-volt fans.

Copyright:
==========
You MAY distribute this TRACK, provided you include this file, with
no modifications.  You may distribute this file in any electronic
format (BBS, Diskette, CD, etc) as long as you include this file 
intact.
