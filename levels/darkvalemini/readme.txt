Information
-----------------
Track Name:	Dark Vale Forest - Mini
Length:		346 meters
Difficulty	Hard
Author:		Saffron
Challenge time:	0:22:500 (Stock Pros)


Description
-----------------
Dark Vale Forest Speedway is a lengthy, technical racetrack, located somewhere in the Central European wilderness.

It recently received major renovations, and now has two additional layouts to support a wider array of motorsports categories.

This is the shortest layout, skipping most of the track after the second hairpin, rejoining the latter part of the West section.

Unlike the longer variants, you get no breathers outside of the start/finish straight. It's a wombo-combo of twists and turns throughout this time.


Requirements
-----------------
Please use a recent RVGL patch.

The track is best experienced using the Gran Turismo 1 soundtrack, which you can download and unzip into your main Re-Volt folder here:

https://drive.google.com/file/d/1wFyudwJVgpZ8KeQBeEH8gFYTOUvlSVWv


Credits
-----------------
Andor - raceline
Blender Foundation - Blender
Huki - Blender addon, RVGL
Lo Scassatore - textures
Marv - Blender addon, textures
Polyphony Digital - textures, inspiration
Rick Brewster - Paint.NET
RVGL Team - RVGL
RVTT Team - textures
HDRI Haven, Ronyx69 - Skybox

Permissions
-----------------
You may not redistribute or modify this track. You may not bundle this track into any content pack or mod I am not involved with. You may not archive older versions of this track and redistribute those either.

If you wish to convert this track to another game, please get in touch with me beforehand.