Information
-----------------
Track Name:	Amberdawn Hill - Touge
Length:		344 meters
Difficulty	Medium
Author:		Saffron
Challenge time:	0:23:000 (Stock Pros)


Description
-----------------
Daybreak Raceway, situated somewhere in the heart of Britain, was once a well-established racetrack in the early days of motorsports. Unfortunately, as time went on, the ones in charge of the circuit had to file for bankruptcy, leaving the track abandoned for decades.

A very long time later, a rich individual with more money than sense, longing to relive their childhood years of being at the racetrack, decided to inject an exorbitant amount of money into renovating the course, while maintaining its key characteristics that made it memorable in the first place.

They also extended the track by adding two new variants: one to appeal more for endurance racing, and another to appeal more towards junior categories and drifting events.

The shortest variant here immediately hits you with a right-hander chicane after the first corner. The tight hairpins that follow also involve elevation changes much like Touge roads, before rejoining the lattermost section of the main raceline.


Requirements
-----------------
Please use a recent RVGL patch.

The track is best experienced using the Gran Turismo 2 soundtrack, which you can download and unzip into your main Re-Volt folder here:

https://drive.google.com/file/d/1pscpKcC6RBhysEC3v87mHr58O4Bh43gw

Credits
-----------------
Blender Foundation - Blender
Huki - Blender addon, RVGL
Lo Scassatore - textures
Marv - Blender addon, textures
Polyphony Digital - textures, inspiration
Rick Brewster - Paint.NET
RVGL Team - RVGL
RVTT Team - textures
RV community - testing
Jockum Skoglund - Skybox

Permissions
-----------------
You may not redistribute or modify this track. You may not bundle this track into any content pack or mod I am not involved with. You may not archive older versions of this track and redistribute those either.

If you wish to convert this track to another game, please get in touch with me beforehand.