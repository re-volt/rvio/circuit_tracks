Information
-----------------
Track Name:	Touge Mountain - Stage 2
Length:		1357 m (Normal) / 1408 m (Reversed)
Difficulty	Medium
Author:		Saffron


Description
-----------------
While developing Touge Mountain, I realised it would be a good idea to split up each half of it into separate tracks additionally, alongside the full variant.

The latest version of RVGL is required for the track to function properly.


Credits
-----------------
Polyphony Digital, Lo Scassatore, Marv and the rest of the Internet for the textures
T-Rider, Kirioso and Odie for the music
Blender Foundation for Blender
Marv, Martin and Huki for the Blender plug-in
Huki, Marv and FranklyGD for RVGL
Kiwi, Marv and R6TE for finding the sound effects used
ARM for the camera nodes tutorial
Rick Brewster for Paint.NET
Everyone in Re-Volt Discord for testing out of the track and giving feedback


Permissions
-----------------
This track may not be distributed anywhere else outside of Re-Volt Zone and Re-Volt I/O content packs.