Information
-----------------
Track Name:	Super Speedway
Length:		454 meters
Difficulty	Easy
Author:		Saffron
Challenge time:	23:300 (Stock Pros)


Description
-----------------
The oval featured in Gran Turismo 2 and 3, in Re-Volt. Keeping to the inside as much as possible is the key here to gain the upper hand.


Requirements
-----------------
You MUST use the latest RVGL patch to see the textures, and for the custom properties to work.


Credits
-----------------
The Internet and Polyphony Digital for the textures
The Blender Foundation for Blender
FBV and/or Lo Scassatore for the custom sounds
Marv, Huki and Martin for the Blender plug-in
The RVGL Team for RVGL
ARM for the camera nodes tutorial
Rick Brewster for Paint.NET


Permissions
-----------------
You may not redistribute or modify this track. You may not bundle this track into any content pack I am not involved with. You may not archive older versions of this track and redistribute those either.