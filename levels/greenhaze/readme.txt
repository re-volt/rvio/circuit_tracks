Information
-----------------
Track Name:	Green Haze Valley
Length:		572 meters
Difficulty		Medium
Author:		Saffron


Description
-----------------
A high-speed circuit with enough hairpins and technical sections to complement the flow of the raceline, and to separate the good from the best.


Requirements
-----------------
You MUST use the latest RVGL patch for the custom properties to work.


Credits
-----------------
The Internet and Polyphony Digital for the textures
Ronyx69 for the skybox
The Blender Foundation for Blender
Marv and Huki for the Blender addon
The RVGL Team for RVGL
ARM for the Camera nodes tutorial
Rick Brewster for Paint.NET
Frizk and Naru for playtesting


Permissions
-----------------
This track cannot be distributed anywhere except my website, or the I/O Circuit Pack.