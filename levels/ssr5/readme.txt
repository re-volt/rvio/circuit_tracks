Information
-----------------
Track Name:	Special Stage Route 5
Length:		723 meters
Difficulty		Hard
Author:		Saffron
Challenge time:	42:700 (Stock Pros)


Description
-----------------
One of the original mainstays of the classic Gran Turismo games makes its way into Re-Volt. Maintaining your speed through the crucial sections of this high-speed track is what you'll want to achieve to keep your distance between yourself and the rest.


Requirements
-----------------
You MUST use the latest RVGL patch to see the textures, and for the custom properties to work.


Credits
-----------------
The Internet and Polyphony Digital for the textures
Scloink (I think) for the truck model
The Blender Foundation for Blender
Marv for his Blender plug-in
Huki and Marv (again) for RVGL
ARM for the Camera nodes tutorial
Rick Brewster for Paint.NET


Permissions
-----------------
You may not redistribute or modify this track. You may not bundle this track into any content pack I am not involved with. You may not archive older versions of this track and redistribute those either.