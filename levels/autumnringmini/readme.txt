Information
-----------------
Track Name:	Autumn Ring Mini
Length:		244 meters
Difficulty		Medium
Author:		Saffron
Challenge time:	16:000 (Stock Pros)


Description
-----------------
Alongside Autumn Ring also exists its shorter variant, perfect for more hasty racing. Every corner matters - make sure to maximise your pace in every corner to gain an upper hand against the opposition.


Requirements
-----------------
The latest RVGL patch is required for the track to function properly.


Credits
-----------------
The Internet and Polyphony Digital for the textures
The Blender Foundation for Blender
Marv and Huki for the Blender addon
The RVGL Team for RVGL
ARM for the Camera nodes tutorial
Rick Brewster for Paint.NET


Permissions
-----------------
You may not redistribute or modify this track. You may not bundle this track into any content pack I am not involved with. You may not archive older versions of this track and redistribute those either.