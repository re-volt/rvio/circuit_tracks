MATERIAL {
  ID              0                             ; Material to replace [0 - 26]
  Name            "DEFAULT"                     ; Display name

  Skid            false                          ; Skidmarks appear on material
  Spark           false                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      false                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           false                         ; Material emits dust

  Roughness       1.000000                      ; Roughness of the material
  Grip            1.000000                      ; Grip of the material
  Hardness        1.000000                      ; Hardness of the material

  DefaultSound    100                             ; Sound when skidding
  SkidSound       100                             ; Sound when skidding
  ScrapeSound     100                             ; Car body scrape [5:Normal]

  SkidColor       192 192 192                   ; Color of the skidmarks
  CorrugationType 0                             ; Type of bumpiness [0 - 7]
  DustType        0                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}

MATERIAL {
  ID              1                            ; Material to replace [0 - 26]
  Name            "MARBLE"                      ; Display name

  Skid            true                          ; Skidmarks appear on material
  Spark           true                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      true                          ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           true                          ; Material emits dust

  Roughness       0.900000                      ; Roughness of the material
  Grip            500.000000                    ; Grip of the material
  Hardness        1.000000                      ; Hardness of the material

  SkidSound       7                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       200 200 200                      ; Color of the skidmarks
  CorrugationType 2                             ; Type of bumpiness [0 - 7]
  DustType        1                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}

MATERIAL {
  ID              2                            ; Material to replace [0 - 26]
  Name            "STONE"                       ; Display name

  Skid            true                          ; Skidmarks appear on material
  Spark           true                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      true                          ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           true                          ; Material emits dust

  Roughness       0.700000                      ; Roughness of the material
  Grip            200.000000                    ; Grip of the material
  Hardness        0.400000                      ; Hardness of the material

  SkidSound       7                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       210 198 125                    ; Color of the skidmarks
  CorrugationType 3                             ; Type of bumpiness [0 - 7]
  DustType        3                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}

MATERIAL {
  ID              3                             ; Material to replace [0 - 26]
  Name            "WOOD"                     ; Display name

  Skid            true                          ; Skidmarks appear on material
  Spark           true                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      false                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           false                         ; Material emits dust

  Roughness       1.000000                      ; Roughness of the material
  Grip            1.000000                      ; Grip of the material
  Hardness        1.000000                      ; Hardness of the material

  SkidSound       6                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       192 192 192                   ; Color of the skidmarks
  CorrugationType 0                             ; Type of bumpiness [0 - 7]
  DustType        0                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}

MATERIAL {
  ID              4                            ; Material to replace [0 - 26]
  Name            "SAND"                       ; Display name

  Skid            true                          ; Skidmarks appear on material
  Spark           true                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      true                          ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           true                          ; Material emits dust

  Roughness       1.000000                      ; Roughness of the material
  Grip            1000.000000                    ; Grip of the material
  Hardness        1.000000                      ; Hardness of the material

  SkidSound       7                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       210 198 125                    ; Color of the skidmarks
  CorrugationType 3                             ; Type of bumpiness [0 - 7]
  DustType        3                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}

MATERIAL {
  ID              5                             ; Material to replace [0 - 26]
  Name            "PLASTIC"                     ; Display name

  Skid            true                          ; Skidmarks appear on material
  Spark           true                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      false                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           false                         ; Material emits dust

  Roughness       1.000000                      ; Roughness of the material
  Grip            1000.000000                    ; Grip of the material
  Hardness        1.000000                      ; Hardness of the material

  SkidSound       6                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       192 192 192                   ; Color of the skidmarks
  CorrugationType 0                             ; Type of bumpiness [0 - 7]
  DustType        0                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}