Track Name: Test Course 6 - Fast Concrete
Folder Name: ...\levels\tc6
Track Type: EXTREME - based Jimk's DirtKit
Author/s: The Me and Me
  Email: saver@gmx.li
  homepage: http://www.themeandme.de/
Length: 1290 meters
Known Bugs: None

Install: Unzip with "Use folder names" on to the main RV folder

Tools used: PSP 7.0; Glue by ali, Christer and Gabor Varga;
	  : DirtKit by Jimk; ZModeler

================================================================
Description
================================================================
Sixth track in a series of tracks we made to test different
things on our cars. While the first 4 got deleted soon after
discovering MAKEITGOOD for testing some of those things,
this one and its icy brother were kept.
Now, nearly 3 years after these 2 were made, SuPeRTaRD asked us
to release em. So we did a bit of optimizing and here they are.
Have, well, fun and hopefully a fast car!

================================================================
Tips
================================================================
Well... Drive it once, and you'll see that you don't really need
any tips to be fast.

================================================================
Thanks And Accolades
================================================================
We want to thank you, for downloading this track, and supporting
the Re-Volt community with dlding it. Then we gotta thank the
whole RV community, too. Its probably the best community on a
racing game all around the web, and it's hard work to get there.
Now we all have to work even harder to keep it there! Thanks
and: Keep it up!

We want to thank Wayne LeMonds, for his great site and forum. If
it Wayne would not have been here, you would possibly not read
this, cuz we never would have gone so deep into Re-Volt without
the forum. Thanks a whole bunch, Wayne! If you havn't visited
his website please do so, you won't regret it.
http://www.revoltdownloads.com/

We want to thank the whole staff of RVA for their great site. It
is the perfect addition to Racerspoint and will be (or probably
is already) the completest Re-Volt site on the web, with all the
cars, tracks, tutorial, tools etc. If you don't know it yet, you
really have to hurry to get there: http://www.rvarchive.com/

We would also like to thank ALL the peeps at #re-volt chat on
the IRC austnet servers for their support and help without
forgetting anyone of them. Thanks dudes!

Then we want to thank all the creators of the tools we used.
Without their hard work, this track would not have been possible.

================================================================
Individual Thanks On This Track
================================================================
Jimk:
Thanks for the DirtKit once more, even tho some of it was
changed during the make of this, it wouldn't have been possible
without you! Thanks!

================================================================
Copyright / Permissions
================================================================ 
You MAY distribute this TRACK, provided you include this file,
with no modifications.  You may distribute this file in any
electronic format (BBS, Diskette, CD, etc) as long as you
include this file intact.

================================================================
Where to get this TRACK
================================================================
Websites : http://www.revoltdownloads.com/
         : http://www.rvarchive.com/
	 : http://www.revoltunlimited.co.uk/
	 : http://www.themeandme.de/