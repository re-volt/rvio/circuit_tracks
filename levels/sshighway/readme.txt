

-------------------------------------------------------------------SS Highway Re-Volt--------------------------------------------------------------------------

----------------------------------------------------------------------by Crescviper----------------------------------------------------------------------------------



General nformation:
------------

Name:          SS Highway Re-Volt

Author:         Crescviper

Type:            Extreme

Length:         1495 m.

Difficult:        9/10

-------------
Installation:
-------------

You must unzip the SSHighway.zip into your main Re-Volt directory.

------------
Descrizione (italiano):
------------

Questa � la mia ultima (e migliore xD) pista fatta con gli ASE TOOLS...ispirata all'autostrada della citt� di Salerno.

La pista si articola principalmente tra l'autostrada SS Highway Re-volt, SS Route Re-volt (Sud) e SS Route Re-volt (Est)

Dedico questa pista (da come si pu� vedere dalla statua nella rotonda) al mio defunto nonno Agostino :'(



------------------------------------------------------------------------------------------------------------------
------------

Description (English):
------------

This is my last (and best xD) track that i made in re-volt with ASE TOOLS...inspired by Highway of the city Salerno

The track is divided mainly between the SS Highway Re-Volt, SS Route Re-Volt (South) and SS Route Re-Volt (East)

I dedicate this track (as can be seen from the statue in the round) to my deceased grandfather Augustine: '(

----------------------------------------

Programs used by me for make this track:

----------------------------------------

3dsmax
ASE TOOLS
Makeitgood
Gimp (for the effects)
Zmodeler
Paint of windows XP
Wolfr4 by Jigebren (to enable the skymap and the MP3)

---------------------------------------

Special thanks to:
The italian site "Aliasrevoltmaster" for the fantastic support of my friends :D
The english site "RVL" (specially Kay The Falcon aka KDL and Mythic Monkey) :D
The english site "RVZT" :D


Copyright by Crescviper 2012...All right reserved
