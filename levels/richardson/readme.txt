﻿12:16 27/12/2022

    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ A creation by Skarma ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

	     	        <Richardson Raceway>

＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Preface)

This track requires RVGL to function properly. It is not and never will be compatible with the original version or 1.2. If you're still using these.... Why?
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Information)

Date	  : 08/05/2022 - 27/12/2022
Track Name: Richardson Raceway
Track Type: Conversion
Folder    : ...\levels\richardson, ...\gfx\
Install   : Put the levels and gfx folders into your Re-Volt folder.
Length    : 466 metres (normal & reversed)
Challenge : 30:500 (normal), 30:250 (reversed) - Stock Pros
Difficulty: Hard

After over ten years, this is my first properly scratch made creation for Re-Volt, made using bezier curves. I wanted to create something akin to Saffron's circuit tracks. It's almost impossible to believe that within a span of just forty five minutes, I went from knowing nothing to having a simple layout.

The race line is very simple with no elevation changes but it still proves to be very challenging when trying to get a fast lap down. To quote Saffon: "It's impressive how a 2 second MS Paint squiggle turned out to have a deceptively difficult raceline."

I'll admit that there are still some things that could've been done, such as shading, shadows and a little better AI. But I'm happy with it as a first track.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Construction)

Base           : Original scratch model by yours truly, textures and assets from Saffron's circuit tracks
Editor(s) used : Blender 2.74 + Jigebren's RV Plugin, Paint.net, Notepad, MAKEITGOOD, Sanity and patience
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Thanks)

Saffron for her invaluable guidance and everything relating to the rest of the track creation, including the rest of MAKEITGOOD, AI nodes, doing the reverse variant, the use of her textures and assets and just putting up with me in general throughout the whole process. If I missed something here, sorry.
Jigebren for his Blender plugin.
The Blender Foundation for Blender.
Rick Brewster for paint.net.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
(Copyrights)

As of 2022, you may still do as you want as long as it isn't some five minute shit hackjob. However, I reserve the right to deny or disallow any modifications to my creations, have them added to any packs, etc, at any time and for any reason that I see fit. This is a blanket usage across all of my cars, from the oldest right up to the new, regardless of what the readmes of previous cars state. If you have a problem with that... I don't care. Do one.

I would really prefer if you did not modify the track to remove the repos for whatever reason. If you don't like it then stop skill issuing yourself and get better.

You may not upload any of my creations to the Steam Workshop.

Converting this track to Roblox is not allowed.

If you use any of my conversions and you do not credit me and the original author(s), you're a cunt. End of story.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
(Where to find this car)

Re-Volt World - https://www.revoltworld.net/
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Disclaimer)

If this track somehow breaks your PC... Not my problem.

Use RVGL. WolfR4 and 1.2 are long obsolete. Nor will I make any of my tracks compatible with the Steam version.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
Have fun, or don't...