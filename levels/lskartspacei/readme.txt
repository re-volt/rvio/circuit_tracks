================================================
LS Kart Space I
================================================

Package Name:			LSKartSpaceI.zip
Installation folders:		Re-Volt/levels/LSKartSpaceI
				

Author:				Lo Scassatore
E-Mail:				loscassatore@aliasrevoltmaster.com
website:			http://www.aliasrevoltmaster.com

Package Description:		LS Kart Space is inspired on the 
				original Kart Space tracks from the
				Gran Turismo Series. 

Thanks to:			Acclaim for Re-Volt,
				Polyphony Digital for the original track
				design.
