Information
-----------------
Track Name:	Autumn Ring
Length:		582 meters
Difficulty		Extreme
Author:		Saffron
Challenge time:	37:000 (Stock Pros)


Description
-----------------
One of the mainstays of the classic Gran Turismo games makes it into Re-Volt. Master and refine your raceline to weave your way past your opponents on this highly technical circuit that'll keep you on your toes.


Requirements
-----------------
You MUST use the latest RVGL patch to see the textures, and for the custom properties to work.


Credits
-----------------
The Internet and Polyphony Digital for the textures
Scloink (I think) for the truck model
The Blender Foundation for Blender
Marv and Huki for the Blender addon
The RVGL Team for RVGL
ARM for the Camera nodes tutorial
Rick Brewster for Paint.NET


Permissions
-----------------
You may not redistribute or modify this track. You may not bundle this track into any content pack I am not involved with. You may not archive older versions of this track and redistribute those either.