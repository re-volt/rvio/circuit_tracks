Information
-----------------
Track Name:	Shiftstep Meadow
Length:		349 meters
Difficulty		Hard
Author:		Saffron
Challenge Time:	27:000 (Stock Pros)


Description
-----------------
Originally known as Sideways Sanctuary C, the track has recieved a complete thematic overhaul to be more in line with the rest of the circuit tracks I've made over the years with a more Gran Turismo aesthetic.


Requirements
-----------------
You MUST use the latest RVGL patch for the textures on the track to display properly.


Credits
-----------------
The Internet, the RVTT team and Polyphony Digital for the textures and skybox
The Blender Foundation for Blender
Marv and Huki for the Blender addon
The RVGL Team for RVGL
ARM for the Camera nodes tutorial
Rick Brewster for Paint.NET


Permissions
-----------------
You may not redistribute or modify this track. You may not bundle this track into any content pack I am not involved with. You may not archive older versions of this track and redistribute those either.