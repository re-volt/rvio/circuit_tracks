===============================================================================
                        *** TRACK INFORMATION ***
===============================================================================

Track name		: Small Circuit 2
Filename		: sircuit2.zip
Track length		: 437 meters
Date Completed		: 8-3-2002

Tools Used		: 3D Studio MAX 4.2, Photoshop 6.1, MAKEITGOOD Editor,
			  rvtmod6, rvminis, GrabClipSave, notepad, wordpad
                                      
Author			: Incogbla
Email Address		: juarez@u.washington.edu
Other tracks by author	: ROAR 2001 Carpet Nats, Small Circuit 1, Touge Run 1
===============================================================================

* Description *
---------------
  The stranded cars from SC1 landed on an unknown shore. By then it was
nighttime and pitch dark. The rough impact of land broke the casing of the
track, and the cars escaped into the new world. There was a flicker of light in
the distance, and the newly marooned toys descended upon it.

  The source of the light turned out to be a minature racetrack, rusted and
dingy from abandonment. It was the last project of the unfortunate manager of
the ACME project which produced SC1. He had long since disappeared, and the
track was the only record of him left in the world. The toy cars seemed drawn
to the spirit of the place, and the races began anew, in a world nestled in
darkness...

  Good luck!

* Tips/Hints *
--------------
-Either side of the fork is identical in distance covered, so choosing the side
 you feel most comfortable tackling is fine.

-There are many ways to go up and down the curved ramps without spinning out.
 Be creative, depending on your choice of car you may have to play with the
 brake and throttle, as well as the timing of your turns. Also, the line
 through the turn is often different from that through a flat turn. Try tracing
 the outside.

-When things get too dark/fast, look at the thick yellow centreline for
 guidance.

-The barriers aren't completely vertical, and are quite deadly when hit. Try to
 avoid contact, even at the cost of speed. It's a lot quicker than reposition
 or flipping!

-If you get 'stuck' to a barrier (quite common with some cars), then it's often
 better to just reposition immediately instead of slowly crawling along hoping
 to get free.

-The cones in the upper straight are not in the ideal line, but you can try to
 knock them into the heavily travelled parts and upset the frontrunners. This
 is an advantage that exists even if pickups aren't enabled.

-The middle yellow strip has different traction properties than the rest of the
 road surface.

-Oh, and do try the replays!

* AI *
------
  This track is much wider than SC1 in most places. There is a single
bottleneck section in the narrow ramps. By now I have understood the futility
of tuning the AI to give a consistent challenge to the seasoned human player.
They are especially hopeless in the uneven surfaces of the downward spiral and
the late sweepers. Despite this, I believe that they are finally competent
enough to give you a scare should you make a mistake during a run. Multiplayer
should be far more interesting, isn't it always?

* Manufacturer's Battle *
-------------------------
  The second manufacturer's battle should be much closer thanks to a more open
track. Once again, I piloted an AMW. (In the standings I am AMW)

Note: UZO = Team Humma, TAT = Team AMW, ZEF = Team Cougar, TORK = Team Toyeca

Here are the results:(simulation mode, no pickups, 10 laps, no param changes)
#1: Humma 	(UZO)	04:58:914
#2: AMW		(TAT)	05:11:868
#3: Probe 	(UZO)	05:12:660
#4: Chaser  	(UZO)	05:19:776
#5: GT-S	(TORK)	05:25:538
#6: Toyeca 	(TORK)	05:27:393
#7: Toyota	(TORK)	05:40:068
#8: CMW		(TAT)	05:49:442
#9: Big Cat	(ZEF)	05:55:457
#10:DMV		(TAT)	06:17:105
#11:Wazoo   	(ZEF)	06:30:756
#12:Cougar  	(ZEF)	06:34:537

  UZO once again proved their dominance, and TORK moved past ZEF and TAT to
claim runner's-up honors. There are less opportunities for upsets in this open
track (as compared to SC1) especially with no pickups. The UZO was deadly in
its consistency, with all three of its cars in a compact formation leading the
field. The others made mistakes in one area or another and became scattered.

  Under skilled human guidance, any car in this class can be competitive. I
believe that the TAT model is quite stable in most of the inclined turns which
can be a major advantage, but it is weakened by slow acceleration. A stable
line is essential.

* Bugs/Problems *
-----------------
-There WAS a problem with the camera getting stuck underneath the car when
 repositioned after falling off the track. This bug is no more. However, in the
 rare case it does happen, hit F1 three times and you will be returned to the
 original view. (This only applies if you use the default view) The brown
 pictorial sign on the main straight hopefully details this in a clear manner.

-Certain cars (user made mostly) get stuck sometimes on the barriers and never
 reposition themselves. This is a car problem, not the track. Thankfully this
 occurs rarely. If a certain car has a recurring problem with getting stuck in
 this fashion, consider using a different .hul for it.

-The replay cameras switch oddly quite often. This is most likely due to the
 track layout, which turns back on itself and thus exposes the car to earlier
 cameras enough that they turn on, even though it may be a watchpoint and not
 show the current car at all. However this provides a nice effect oftentimes,
 because other cars often will zoom by.

-There are NO reposition triggers on the track! So if you fall off/think you're
 going to go overboard, it's up to you to hit reposition as quick as you can.
 Else you must wait for RV to automatically reposition you which takes a while.
 But be warned, cars often ride the rail and plop back into the track on their
 own.

-During replays, if a car fall off the track and the screen goes pitch dark,
 the screen may stay black for the duration of the replay, even though it is
 clearly continuing (can hear sounds, etc). Try tapping pgup/pgdn repeatedly.
 If that fails, all you can do is to restart the replay. To avoid possibly a
 second occurrence you can quickly switch away from the car when it's about to
 fall off using pgup/pgdn. Sometimes, if you wait during the pitch darkness,
 the view comes back on if the car falls off again and has to reposition. If
 you were in the middle of a long replay that might be handy to know.

* Installation *
----------------
  Unzip sircuit2.zip into the root Re-Volt folder. The appropriate directories
will be created automatically.

* Copyright/Permissions *
-------------------------
  This track may only be distributed for FREE, which means it cannot be included
in any website that charges a fee for access to the track's download nor can it
be placed on a CD which is sold for profit. Plus, all files, including this
README, must be intact within the sircuit1.zip archive when it is distributed.

  Other track authors MAY use this track as a base to create others. However,
this README file MUST be retained INTACT in addition to any readme files you
may create. The new readme must also state that this track was the base for
the new track.

  All models/textures created from scratch by Incogbla, with the exception of
the wobbly cones and strobes. That's Acclaim/Probe's work.

* Thanks and credits go to *
----------------------------
Too many to thank, you know who you are.
The pioneers who gathered the information that made extreme editing possible
Everyone in #revolt-chat
Helpful folks on the racerspoint.com boards
Authors of RV track utilities
People behind rvarchive.com
Acclaim/Probe for Re-Volt, quite possibly the finest game ever created.
And, of course, yourself for keepin' it real.

* Translator Credits *
----------------------
German - The Me & Me
Spanish - rst
Italian - Gandalf
Portuguese - PauloBecker
Hungarian - bootes
French - Antimorph
Finnish - arto
Japanese - Yamada
Swedish - Ted Borg
Chinese - color0
Chinese - Ninjagaiden
English - shawn064 (?)