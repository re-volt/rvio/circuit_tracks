Track name: KEITUNE SPEEDWAY
Install in folder: Re-volt\levels\Keitune
Author: Mace (aka: Mace121)
Email Address: Macethe0mni@gmail.com


Description:

Kei-Tune. A Japanese RC racing circuit that's been around since 1996 in Tsuchiu, Ichihara, Chiba, Japan (mouthful). It is a short course, with a length of ~307m. On this map, it is now 407m, as a recreation. 

The course's known claim to fame (in media) was one-half of the 2 final race tracks in Taito's 2000 RC arcade Racing game, RC De Go! Personally, it was the toughest track I've ever raced on, notably using extremely fast & tight cars and the course had so many quick turns. Difference between the RCDC & the version you're playing is the layout (even though the game version was 20 years old at this point). This version's layout was traced from a Google Maps screenshot, which the new 2016 layout.

RC Racing tracks aren't as well documented like other motorsport tracks like Brands Hatch or Daytona, but there's no shortage of videos of RC cars racing on these tracks. Also, this may not be the prettiest track I've done, but I did had some fun making this.


______________________________________________________________________________________

Some Info:

-It's a flat track, but with pit lane & some walls
-Only a few pickups are placed

Music:
-RC De Go: C/D Speedway
-RC De Go: Desire to Win


Mixtape (in-development):
-Celldweller: Lost in Time
-Andy Hunter: Go (6-min version)
-Zack Hemsey: Don't Get In My Way
-Smuggler's Run 2: Human Bond
-Jet Set Radio: That's Enough
-Toxic Avenger: My Only Chance
-Alice Deejay: Got to Get Away
-Megadeth: Hangar 18
-Metal Gear Rising: I'm My Own Master Now
-Aviators: Fear of Flight
-Sabaton: Great War


Misc.

The Track may not be geographically accurate to the real life thing.

______________________________________________________________________________________

* Copyright / Permissions *

Skybox by KIIRA
Grass textures by Kenny.nl
Warclub textures from Acclaim Studios Austin, model by me.

Yeah, sure. Why not, unless you credited the author(s).
