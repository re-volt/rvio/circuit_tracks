  ______           _       _   _   
 |  ____|         | |     (_) | |  
 | |__      __ _  | |__    _  | |_ 
 |  __|    / _` | | '_ \  | | | __|
 | |      | (_| | | |_) | | | | |_ 
 |_|       \__,_| |_.__/  |_|  \__|
                                   
                                      	&

+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+
| #######                      ######   #     #  #            #######                         |
|    #     #    #  ######      #     #  #     #  #               #     ######    ##    #    # |
|    #     #    #  #           #     #  #     #  #               #     #        #  #   ##  ## |
|    #     ######  #####       ######   #     #  #               #     #####   #    #  # ## # |
|    #     #    #  #           #   #     #   #   #               #     #       ######  #    # |
|    #     #    #  #           #    #     # #    #               #     #       #    #  #    # |
|    #     #    #  ######      #     #     #     #######         #     ######  #    #  #    # |
+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+
 _____
[Index]___________
|จจจจจจ		  \
[]  General 	  /
[]  Description   \
[]  History	  /
[]  Description   \
[]  Tools and res /
[]  RVL team      \
[]  Contact       /
|_________________\



 _______
(General)
 จจจจจจจ
	Track Name: 			GPX
	จจจจจจจจจจ                         
	Folder Name:			GPX
	จจจจจจจจจจจจ 
	Creator(s):		 	Fabit (2005)
	จจจจจจจจจจ			The RVL Team (2011-2012)

	Project director:		Zipperrulez
	จจจจจจจจจจจจจจจจจ
	Track Type:			Extreme (instance-based)
	จจจจจจจจจจจ
	Track Length:			1394m
	จจจจจจจจจจจจจ
	Skymap:				YES (Dave-o-rama)
	จจจจจจจ




 ___________
(Description) 
 จจจจจจจจจจจ
GPX is just like GP1, a fast track suitable for high-speed cars (50+ MPH).	


 _______
(History)
 จจจจจจจ
	After the success of GP1, Fabit started its sequel (codenamed GPX) on April'16th 
2005.Yet it was never finished and models were just impossible to be found.

	Later on August'8th 2011, Zipperrulez uploaded it to the RVL HQ place, jobs we split 
and people started working on it. Yet, due to real life issues, the track has been put on hold
until Summer'2012.




 _____________
(Tools and Res)
 จจจจจจจจจจจจจ
PRM KIT : JimK's kit, prmbend
Fabit's : Rv Glue, Rv Remap, PRM2NCP, Rv Minis
RVL's	: 3Ds max, Paint.NET, WolfR4, RV Organizer



 ________
(RVL Team) 
 จจจจจจจจ

==========================================================================================
!Note: Fabit has modeled, directed, made and checked textures and done some of makeitgood.
==========================================================================================

	Director:			Zipperrulez
	จจจจจจจจจ
	Modelers:			Kfalcon, Nero
	จจจจจจจจจ
	Texture Quality Check:		Zipperrulez, Kfalcon, Halogaland
	จจจจจจจจจจจจจจจจจจจจจจ
	MAKEITGOOD:			Dave-o-rama, miromiro, Kfalcon
	จจจจจจจจจจ
	Custom levelintro:		Dave-o-rama
	จจจจจจจจจจจจจจจจจจ
	Testers:			Whole team , especially Zipperrulez
	จจจจจจจจ

Also:
จจจจจ
- R6TurboExtreme
- krisss
- Burner94
- Citywalker
- Skarma

And:
จจจจ
- urbanrocker
- Kajeuter
- RV_Passion
- MythicMonkey





 _______
(Contact)
 จจจจจจจ

	Email
		(Fabit) fab60@libero.it
		( RVL ) revoltlive@gmail.com

	Site:
		http://z3.invisionfree.com/Revolt_Live
	