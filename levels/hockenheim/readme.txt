=====================================================================
Hockenheimring by scloink. A Sports Car GT conversion
=====================================================================
Track Name              : Integra Type R and Integra Type R GT
 
Install in folder       : Unzip to your main ReVolt folder and all files will go where needed

Author/s                : scloink 

Email Address           : extremerv@djis.net 

Homepage	        : http://www.rvarchive.com/                                                         

====================================================================
Description:
I very fast track suitable for the Super Pro class of cars. The F1 cars work great on here so I would recommend getting them.  


Creation:
Well I used the tools MASpuce and VRL2vrml to convert the track to the wrl format. I then imported it into max for texturing. I had to delete alot of stuff to make it work with ReVolt so if you have played SCGT you will notice alot missing. The poly structure was such that I could not texture it too entirelty well because of the way RV reads the UVW's.

I tried using Farce Fields to slow the cars when you went off the track and it worked to an extent. It got really hard to make them do what I wanted in the turns. You will notice that i took some liberties with walls and barriers to black any attempts at shortcuts. I know if you look at this track you will find alot of texture problems and other things but this is due to the above mentioned reason about the mesh structure. 


Tools Used:
MASpuce
VRL2vrml
3ds Max 
PSP 7.0

Thanks:
Thanks go out to the entire community and especially to Wayne Lemonds because without his support during GA Games romp through our lovely community the RV scene would have probably died because his message board helped keep people around and interested.

Special Thanks:
Electronic Arts for their mesh and textures.
Cybinary - Testing, POSnodes, AInodes, Track Zones, and Pickups.
The Me and Me - Testing 
SlowJustice - Testing 
DSL_Tile - Testing 
SuperTard - Testing 
triple6s - Testing 
srmalloy - Testing
LaserBeams - Testing
Anyone else I may have forgotten feel free to reprimand me on the forum.


Visit:
www.rvarchive.com - The Revolt Archive
www.racerspoint.com - The Racers Point, home of the greatest forum anywhere


