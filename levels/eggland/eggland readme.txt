Eggland V0.2
A GeneRally track for ReVolt
By Strava 04/08/2012

Type: Tarmac Road Course
Length: 218 Meters
Difficulty: Easy

I originally created this track for a fun table top racing game called GeneRally.
Later I created the track "to scale" for rFactor.
This is a conversion of the rFactor track.

Change Log:
04/08/2012 Initial release
04/09/2012 Added Pickups