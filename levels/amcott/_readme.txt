+++++++++++++++
+++ AMCO TT +++
+++++++++++++++


Informations
============

Track name:		AMCO TT
Length:			550 m


Installation
************

Install folder:		AMCOTT
Installation:		Extract AMCOTT.zip into your Re-Volt folder.
Uninstallation:		Delete <Re-Volt dir>\levels\AMCOTT
			Delete <Re-Volt dir>\gfx\AMCOTT.bmp


Description
===========

This track is a REALISTIC REPRODUCTION of an authentic remote controlled car track
located in Brest - France.
The real AMCO complex includes a speed track and a cross track. This is the revolt 
adaptation of the AMCO cross track.
The track takes 3 different surfaces and contains jumps, double jump and an american table.


Reference
=========

AMCO (Auto Model Club de l'Ouest) : "http://amco.rvtt.com/"

Clamour is managing the site of this complex and he has notably participated in the revolt project
by pointing pertinent informations about the track.
The rest of the team is built from the Tip Top community : "http://www.rvtt.com".


Authors
=======

smag : General track design and texturing.
toto : Sponsors objects and textures.
CD   : First version of AMCO building.
TF & smag : Final version of AMCO building.
SebR & smag : MAKEITGOOD work.
Clamour : Beta test & AMCO references.


Playing reversed
****************

If you like AMCO TT, you can also play it backwards.
Select an original track, reverse it (play as "TRACKER" to have privileges),
and select AMCO TT.


Copyright / Permissions
=======================

You may distribute this track, provided you include this file, with no modification.
you can use any part of this package for you own work provided you do it for free and
mention original authors.


Enjoy...

Romeo [smag]