Tingin Inc. GP by Zeino (Tryxn on RVZ)

hi there, made a new track wooo, as you might have noticed the main sponsor is Tingin Incorporated. are you skilled enough to put a decent challenge against the competition in the tricky parts of the factory? inspired by one place i went around on my bus the other week. i hope you enjoy!

featuring:
- practice star
- time trial time to beat
- decent challenge

credits:
the song is Waterfall by Vista-Sound13 https://www.newgrounds.com/audio/listen/1068403
textures: various sources (Saffron's Dark Vale Forest, some textures edited by me, stickers/sponsors credit goes to Yukes von Faust, Phimeek, thatmotorfreak and Shara from their sponsor releases in the #resources channel in the discord)
truck model: taken from Saffron's Special Stage Route 5
the smoke particles: edited from Kiwi's Spa-Volt
sounds: 
taken from Saffron's Dark Vale Forest,
birds sound by klankbeeld on freesound.org (https://freesound.org/people/klankbeeld/sounds/530415/)
skybox: taken from Rooftop Chase: Redux

Special thanks:
Saff, my friends At Home and you idk lol