Information
-----------------
Track Name:	Red Rock Valley
Length:		790 meters
Difficulty		Easy
Author:		Saffron
Challenge Time:	41:750 (Stock Pros)


Description
-----------------
The high-speed circuit exclusive to Gran Turismo 2 finally makes it into Re-Volt. Blast through the sunset skies as you navigate your way past your opponents on this full throttle speedway.


Requirements
-----------------
You MUST use the latest RVGL patch to see the textures, and for the custom properties to work.


Credits
-----------------
The Internet and Polyphony Digital for the textures
Ronyx69 for the skybox
The Blender Foundation for Blender
Marv and Huki for the Blender addon
The RVGL Team for RVGL
ARM for the Camera nodes tutorial
Rick Brewster for Paint.NET


Permissions
-----------------
You may not redistribute or modify this track. You may not bundle this track into any content pack I am not involved with. You may not archive older versions of this track and redistribute those either.