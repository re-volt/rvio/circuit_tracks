===============================================================================
*** THIS TRACK IS FOR USE WITH THE TVTIME cheat code.....
===============================================================================
Track name              : TVGP
Author                  : Skitch2 and Manmountain.
Email Address           : Manmountain: revolt_car_support@blueyonder.co.uk
Filename                : TVGP.zip
Track length            : 301m
Textures (if any)      	: Borrowed from Hockenhiemring
Release Date            : 22nd May 2005

Additional Credits to	: Many thanx to Manmountain for getting involved with this one .
			: Many thanks to Skitch2 for a great job in the time taken.

Bitume patch by SebR

===============================================================================


* Description *
---------------
Basicly, this is a track i made in 1 hour in max for a bit of fun.
I think that it came out quite cool and is a blast to race.

Max work, lighting By SKITCH2
RV editing: Zones, AI And stuff all done by Manmountain.

** THIS TRACK IS FOR USE WITH THE TVTIME cheat code **
Type TVTIME as your name in the game wheel.

* Tips/Hints *
--------------
Try not to cut the corners, but you can stray slightly wide.

* Installation *
----------------
Install: Extract to you RV Dir....

* Copyright/Permissions *
-------------------------
All the best and look out for more fun tracks from me in the near future!!!