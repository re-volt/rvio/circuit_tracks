Information
-----------------
Track Name:	Motor Sports Land
Length:		174 meters
Difficulty		Extreme
Author:		Saffron
Challenge time:	16:000 (Stock Pros)


Description
-----------------
This karting track exclusive to Gran Turismo 2, now in Re-Volt. Navigating your way through the neverending barrage of twists and turns is the key to gaining the upper hand on this incredibly hectic circuit.


Requirements
-----------------
You MUST use the latest RVGL patch for the custom properties to work.


Credits
-----------------
The Internet, the RVTT team and Polyphony Digital for the textures and skybox
Mace for some of the textures on Keitune Speedway
The Blender Foundation for Blender
Marv and Huki for the Blender addon
The RVGL Team for RVGL
ARM for the Camera nodes tutorial
Rick Brewster for Paint.NET


Permissions
-----------------
You may not redistribute or modify this track. You may not bundle this track into any content pack I am not involved with. You may not archive older versions of this track and redistribute those either.